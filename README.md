Nottery
=======
## About
This application is to notify about the battery status through your default browser.
## Usage
Open the **index.html** file in your browser to experience the process.
## Credits
Thanks to [Mozilla Developer Network](https://developer.mozilla.org)
