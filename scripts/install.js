
var button = document.getElementById('install');


if('mozApps' in navigator) {
    
    
    var manifest_url = location.href + 'manifest.webapp';
    
    function install(ev) {
      ev.preventDefault();
     
      var installLocFind = navigator.mozApps.install(manifest_url);
      installLocFind.onsuccess = function(data) {
        
      };
      installLocFind.onerror = function() {
        
        alert(installLocFind.error.name);
      };
    };
    
    
    var installCheck = navigator.mozApps.checkInstalled(manifest_url);
    installCheck.onsuccess = function() {
      
      if(installCheck.result) {
        button.style.display = "none";
      } else {
        button.addEventListener('click', install, false);
      };
    };
} else {
  button.style.display = "none";
}
